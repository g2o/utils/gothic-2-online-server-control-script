#!/bin/bash

while true; do
    ./G2O_Server.x64

    exitCode=$?
    
    if [ $exitCode -eq 0 ]; then
        echo "Server was stopped. Exiting script."
        break
    elif [ $exitCode -eq 1 ]; then
        echo "Restarting server..."
    else
        echo "Unknown exit code: $exitCode. Exiting script."
        break
    fi

    for i in 2 1; do
        echo "$i..."
        sleep 1
    done
    echo "Rebooting now!"
done